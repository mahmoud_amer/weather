//
//  ViewController.swift
//  Weather
//
//  Created by Mahmoud Amer on 7/2/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {

    var locationManager: CLLocationManager!
    
    var searching: Bool = false
    var displayMode: displayMode = .userLocation
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var searchCitiesArray: BaseModel = []
    var citiesArray: BaseModel = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        UserDefaults.standard.set(nil, forKey: UserDefaultsKeys.userSelectedCities.rawValue)
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        registerCellNIB()
        prepareCitiesArray()
    }
    
    func prepareCitiesArray() {
        let cities = UserDefaultsHelper.getUserSelectedCities()
        if cities.count > 0 {
            displayMode = .savedCities
            citiesArray = cities
            tableView.reloadData()
        }else{
            displayMode = .userLocation
            if UserDefaultsHelper.getLocationPermissions() {
                showLoading(show: true)
                /* For testing on simulator, send cairo coordinates */
                let location = CLLocation(latitude: 30.0444, longitude: 31.2357)
//        let location = CLLocation(latitude: locationManager.location?.coordinate.latitude ?? 0, longitude: locationManager.location?.coordinate.longitude ?? 0)
                getUserCityByLocation(location: location)
            }else{
                //London
                getUserCityByLocation(location: CLLocation(latitude: 51.5074, longitude: 0.1278))
            }
        }
    }
    
    
    func getUserCityByLocation(location: CLLocation) {
        LocationHelper.getUserCityFrom(location: location, completion: { [weak self] (result) in
            guard let `self` = self else { return }
            self.showLoading(show: false)
            if let city = result {
                self.citiesArray = [city]
                self.tableView.reloadData()
            }
        })
    }
    
    private func showLoading(show: Bool) {
        if show {
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
        }else{
            activityIndicator.isHidden = true
            activityIndicator.stopAnimating()
        }
    }
    
    private func registerCellNIB() {
        tableView.register(HomeTableViewCell.self, forCellReuseIdentifier: "homeCell")
        tableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "homeCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 2 {
            searching = true
            ServicesManager.searchCities(query: searchText) { [weak self] (result) in
                guard let `self` = self else { return }
                if let cities = result, cities.count > 0 {
                    self.searchCitiesArray = cities
                    self.tableView.reloadData()
                }
            }
        }else{
            searching = false
            prepareCitiesArray()
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return searching ? searchCitiesArray.count : citiesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homeCell", for: indexPath) as! HomeTableViewCell
        
        let currentArray = searching ? searchCitiesArray : citiesArray
        let city = currentArray[indexPath.row]
        if let name = city.name, name != "" {
            cell.cityLabel.text = name
            cell.addToCities = { [weak self] in
                guard let `self` = self else { return }
                self.addToCities(city: city)
            }
        }
        cell.addButton.isHidden = !searching
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentArray = searching ? searchCitiesArray : citiesArray
        
        let forecastVC: ForecastViewController = self.storyboard?.instantiateViewController(withIdentifier: "Forecast") as? ForecastViewController ?? ForecastViewController()
        forecastVC.city = currentArray[indexPath.row]
        self.navigationController?.pushViewController(forecastVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) && (!searching)  && (displayMode == .savedCities){
            let currentArray = self.searching ? self.searchCitiesArray : self.citiesArray
            UserDefaultsHelper.removeCity(city: currentArray[indexPath.row])
            self.prepareCitiesArray()
        }
    }
    
    
    func addToCities(city: City) {
        UserDefaultsHelper.addToUserSelectedCities(city: city)
        self.searchBar.text = ""
        self.searching = false
        self.prepareCitiesArray()
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            UserDefaultsHelper.saveLocationPermissions(permitted: true)
        case .denied, .notDetermined, .restricted:
            UserDefaultsHelper.saveLocationPermissions(permitted: false)
        }
        prepareCitiesArray()
    }
}

