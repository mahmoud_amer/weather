//
//  ForecastViewController.swift
//  Weather
//
//  Created by Mahmoud Amer on 7/3/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import UIKit

class ForecastViewController: UIViewController {
    
    var city: City?
    var forcast: [Forecastday]?
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showLoading(show: true)
        cityNameLabel.text = city?.name ?? "Unknown City"
        ServicesManager.getForecast(city: city?.name ?? "") { [weak self] (result) in
            guard let `self` = self else { return }
            self.showLoading(show: false)
            if let forecastResult = result?.forecast?.forecastday, forecastResult.count > 0 {
                self.forcast = result?.forecast?.forecastday
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func showLoading(show: Bool) {
        if show {
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
        }else{
            activityIndicator.isHidden = true
            activityIndicator.stopAnimating()
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ForecastViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forcast?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastCell", for: indexPath)
        cell.textLabel?.text = "date: \(forcast?[indexPath.row].date ?? "") / TMEP:  \(forcast?[indexPath.row].day?.avgtemp_c ?? 0)C"
        
        return cell
    }
}
