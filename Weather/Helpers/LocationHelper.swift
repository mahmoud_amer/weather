//
//  LocationHelper.swift
//  Weather
//
//  Created by Mahmoud Amer on 7/5/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation
import CoreLocation

class LocationHelper {
    public static func getUserCityFrom(location: CLLocation, completion: @escaping (City?)->()) {
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            // Place details
            var placeMark: CLPlacemark!
            if let placeMark = placemarks?[0] {
                // City
                if let city = placeMark.locality {
                    var sublocality = "unknown"
                    if let addressSublocality = placeMark.subLocality {
                        sublocality = addressSublocality
                    }
                    var country = ""
                    if let addressCountry = placeMark.country {
                        country = addressCountry
                    }
                    let cityObject: City = City.init(id: 1, name: city, region: sublocality, country: country, lat: location.coordinate.latitude, lon: location.coordinate.longitude, url: "unknown")
                    completion(cityObject)
                }
            }
        })
    }
}
