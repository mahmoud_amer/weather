//
//  Helpers.swift
//  Weather
//
//  Created by Mahmoud Amer on 7/4/18.
//  Copyright © 2018 Amer. All rights reserved.
//

/*
 IMPORTANT NOTE
 xCode 9 has a bug of saving userDefaults and re-run the app, Your changed wouldn't be saved
 for some issues related to xCode not me :)
 */

import Foundation
import CoreLocation

public enum UserDefaultsKeys: String {
    case locationPermissions = "locationPermissions"
    case userSavedCities = "userSelectedCities"
}

public enum displayMode: Int {
    case savedCities
    case userLocation
}

class UserDefaultsHelper {
    public static let listMaxNumber = 5
    
    public static func saveLocationPermissions(permitted: Bool) {
        UserDefaults.standard.set(permitted, forKey: UserDefaultsKeys.locationPermissions.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    public static func getLocationPermissions() -> Bool {
        return UserDefaults.standard.bool(forKey: UserDefaultsKeys.locationPermissions.rawValue)
    }
}

extension UserDefaultsHelper {
    
    public static func getUserSelectedCities() -> [City] {
        if let propertylistSongs = UserDefaults.standard.array(forKey: UserDefaultsKeys.userSavedCities.rawValue) as? [[String:Any]] {
            return propertylistSongs.compactMap{ City(dictionary: $0) }
        }
        return []
    }
    
    public static func removeCity(city: City) {
        let cities = getUserSelectedCities()
        if cities.count > 0 {
            let newCities = cities.filter({ $0.id != city.id })
            saveCitiesArray(cities: newCities)
        }
    }
    
    public static func addToUserSelectedCities(city: City) {
        var cities = getUserSelectedCities()
        let existed = cities.filter({ $0.id == city.id })
        let maxReached: Bool = cities.count >= listMaxNumber ? true : false
        if (existed.count > 0) || maxReached {
            return
        }
        cities.append(city)
        saveCitiesArray(cities: cities)
    }
    
    internal static func saveCitiesArray(cities: [City]){
        removeKeyValueFromDefaults(Key:UserDefaultsKeys.userSavedCities.rawValue)
        if cities.count > 0 {
            let propertylistCities = cities.map{ $0.propertyListRepresentation }
            UserDefaults.standard.set(propertylistCities, forKey: UserDefaultsKeys.userSavedCities.rawValue)
            UserDefaults.standard.synchronize()
        }else{
            UserDefaults.standard.set(nil, forKey: UserDefaultsKeys.userSavedCities.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    
    internal static func removeKeyValueFromDefaults(Key:String){
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: Key)
    }
}


