//
//  HomeTableViewCell.swift
//  Weather
//
//  Created by Mahmoud Amer on 7/4/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    //MARK - Add To Cities Closure
    var addToCities: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func addButtonClick(_ sender: UIButton) {
        addToCities?()
    }
}
