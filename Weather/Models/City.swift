//
//  City.swift
//  Weather
//
//  Created by Mahmoud Amer on 7/3/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation

typealias BaseModel = [City]

struct City : Codable {
    let id : Int?
    let name : String?
    let region : String?
    let country : String?
    let lat : Double?
    let lon : Double?
    let url : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case region = "region"
        case country = "country"
        case lat = "lat"
        case lon = "lon"
        case url = "url"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        region = try values.decodeIfPresent(String.self, forKey: .region)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
        lon = try values.decodeIfPresent(Double.self, forKey: .lon)
        url = try values.decodeIfPresent(String.self, forKey: .url)
    }
    
    init(id: Int, name: String, region: String, country: String, lat: Double, lon: Double, url: String) {
        self.id = id
        self.name = name
        self.region = region
        self.country = country
        self.lat = lat
        self.lon = lon
        self.url = url
    }
    
    init?(dictionary : [String:Any]) {
        guard let id = dictionary["id"] as? Int,
            let name = dictionary["name"] as? String,
            let region = dictionary["region"] as? String,
            let country = dictionary["country"] as? String,
            let lat = dictionary["lat"] as? Double,
            let lon = dictionary["lon"] as? Double,
            let url = dictionary["url"] as? String
        else { return nil }
        self.init(id: id, name: name, region: region, country: country, lat: lat, lon: lon, url: url)
    }
    
    var propertyListRepresentation : [String:Any] {
        return ["id" : id, "name" : name, "region" : region, "country" : country, "lat" : lat, "lon" : lon, "url" : url]
    }
}

