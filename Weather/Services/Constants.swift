//
//  Constants.swift
//  Weather
//
//  Created by Mahmoud Amer on 7/3/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation

public enum Constants: String {
    case baseURL = "http://api.apixu.com/v1"
    case appID = "f1fde4839cb64ad989a203205180307"
    case forecastPath = "/forecast.json"
    case citiesSearchPath = "/search.json"
}
