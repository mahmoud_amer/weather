//
//  ServicesManager.swift
//  Weather
//
//  Created by Mahmoud Amer on 7/3/18.
//  Copyright © 2018 Amer. All rights reserved.
//

import Foundation
import Alamofire

class ServicesManager {
    public static let forecastDaysNumber = 5
    public static func searchCities(query: String, completion: @escaping (BaseModel?)->()) {
        let endPoint: String = Constants.baseURL.rawValue + Constants.citiesSearchPath.rawValue + "?key=\(Constants.appID.rawValue)&q=\(query)"
        let cacheKey: String = "\(Constants.citiesSearchPath.rawValue)_\(query)"
        
        if let data = CacheHelper.getCachedRequest(cacheKey: cacheKey) {
            print("cached and fetched from cache : \(data)")
            let jsonDecoder = JSONDecoder()
            if let baseModel = try? jsonDecoder.decode(BaseModel.self, from: data) {
                completion(baseModel)
            }
        }else{
            Alamofire.request(endPoint, method: .get, encoding: JSONEncoding.default)
                .responseJSON { response in
                    if let data = response.data, let _ = String(data: data, encoding: .utf8) {
                        if let baseModel = try? JSONDecoder().decode(BaseModel.self, from: data) {
                            CacheHelper.cacheRequestResponse(cacheKey: cacheKey, response: data)
                            completion(baseModel)
                        }
                        
                    }
            }
        }
        
    }
    
    public static func getForecast(city: String, completion: @escaping (ForecastBaseModel?)->()) {
        let endPoint: String = Constants.baseURL.rawValue + Constants.forecastPath.rawValue + "?key=\(Constants.appID.rawValue)&q=\(city)&days=\(forecastDaysNumber)"
        let cacheKey: String = "\(Constants.forecastPath.rawValue)_\(city)_forecastDaysNumber"
        
        let encodedUrl = endPoint.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if let data = CacheHelper.getCachedRequest(cacheKey: cacheKey) {
            print("cached and fetched from cache : \(data)")
            let baseModel = try? JSONDecoder().decode(ForecastBaseModel.self, from: data)
            completion(baseModel)
        }else{
            Alamofire.request(encodedUrl ?? "", method: .get, encoding: JSONEncoding.default)
                .responseJSON { response in
                    if let data = response.data, let _ = String(data: data, encoding: .utf8) {
                        if let baseModel = try? JSONDecoder().decode(ForecastBaseModel.self, from: data) {
                            CacheHelper.cacheRequestResponse(cacheKey: cacheKey, response: data)
                            completion(baseModel)
                        }
                    }
            }
        }
        
    }
}
